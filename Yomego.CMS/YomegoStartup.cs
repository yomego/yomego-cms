﻿using System.Web.Http;
using System.Web.Routing;
using Yomego.CMS.Mvc.Serializing;
using Yomego.CMS.Mvc.Serializing.Converters;
using Yomego.CMS.Mvc.Startup;
using Yomego.CMS.Umbraco.Services.Content;
using Yomego.CMS.Umbraco.Services.DataTypes;

namespace Yomego.CMS
{
    public class YomegoStartup
    {
        public static void Register(RouteCollection routes, HttpConfiguration config)
        {
            // Hook up Umbraco plugin
            App.ResolveUsing<ContentService, UmbracoContentService>();
            App.ResolveUsing<DataTypeService, UmbracoDataTypeService>();

            YomegoRouteConfig.RegisterRoutes(routes);
            YomegoApiConfig.Register(config);

            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new ExcludeContractResolver(new[]
            {
                "Children",
                "ContentSet",
                "ContentType",
                "ItemType",
                "Parent",
                "Properties",
                "properties",
                "this",
                "Content"
            });

            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            config.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.None;
            config.Formatters.JsonFormatter.SerializerSettings.Converters.Add(new HtmlStringConverter());
            config.Formatters.Remove(config.Formatters.XmlFormatter);
        }
    }
}