﻿namespace Yomego.CMS.Constants
{
    public class Examine
    {
        public const string MainExamineIndexProvider = "WebsiteIndexer";
        public const string MainExamineSearchProvider = "WebsiteSearcher";
        public const string MainExamineIndexset = "WebsiteIndexSet";
    }

}
