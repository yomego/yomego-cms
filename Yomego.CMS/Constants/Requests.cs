﻿namespace Yomego.CMS.Constants
{
    public class Requests
    {
        public const string Node = "Node";

        public const string PageId = "pageID";

        public const string ContentType = "ContentType";

        public const string Routed = "Routed";
    }
}