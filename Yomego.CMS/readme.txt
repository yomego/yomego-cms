﻿Thanks for installing the Yomego Umbraco CMS framework.

To full enable this service you will be required to follow this complicat step:

1.  Add the following line to your global asax

	YomegoStartup.Register(RouteTable.Routes, config);

Optionally you can manage Doc Types and Data Types via usync in our custom section

1. Go to the CMs users section and open the account you want to enable this for
2. Grant this user access to the 'yomego' section
3. Refresh the page and enter the section
4. You can now export Doc & Data types via the pull button and sync them on a remote server via the Push button if you deploy them2


Cheers and Enjoy =)