﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Models;

namespace Yomego.CMS.Umbraco.ModelBuilder.ComponentModel.TypeConverters
{
    /// <summary>
    /// Provides a unified way of converting multi node tree picker properties to strong typed collections.
    /// Adapted from <see href="https://github.com/Jeavon/Umbraco-Core-Property-Value-Converters/blob/v2/Our.Umbraco.PropertyConverters/MultiNodeTreePickerPropertyConverter.cs"/>
    /// </summary>
    /// <typeparam name="T">
    /// The <see cref="Type"/> of the node to return.
    /// </typeparam>
    public class MultiNodeTreePickerConverter : BaseTypeConverter
    {
        /// <summary>
        /// Returns whether this converter can convert an object of the given type to the type of this converter, using the specified context.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
        /// <param name="sourceType">A <see cref="T:System.Type" /> that represents the type you want to convert from.</param>
        /// <returns>
        /// true if this converter can perform the conversion; otherwise, false.
        /// </returns>
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string) || sourceType == typeof(int) || typeof(IPublishedContent).IsAssignableFrom(sourceType))
            {
                return true;
            }

            return base.CanConvertFrom(context, sourceType);
        }

        /// <summary>
        /// Converts the given object to the type of this converter, using the specified context and culture information.
        /// </summary>
        /// <param name="context">An <see cref="T:System.ComponentModel.ITypeDescriptorContext" /> that provides a format context.</param>
        /// <param name="culture">The <see cref="T:System.Globalization.CultureInfo" /> to use as the current culture.</param>
        /// <param name="value">The <see cref="T:System.Object" /> to convert.</param>
        /// <returns>
        /// An <see cref="T:System.Object" /> that represents the converted value.
        /// </returns>
        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var list = GetListFromOriginalContextType(context);

            if (value == null)
            {
                return list;
            }

            var s = value as string ?? value.ToString();
            if (!string.IsNullOrWhiteSpace(s))
            {
                int n;
                var nodeIds = XmlHelper.CouldItBeXml(s)
                    ? ConverterHelper.GetXmlIds(s)
                    : s
                        .ToDelimitedList()
                        .Select(x => int.TryParse(x, out n) ? n : -1)
                        .Where(x => x > 0)
                        .ToArray();

                if (nodeIds.Any())
                {
                    // Oh so ugly if you let Resharper do this.
                    // ReSharper disable once LoopCanBeConvertedToQuery
                    foreach (var nodeId in nodeIds)
                    {
                        var item = GetFromId(nodeId);

                        if (item != null)
                        {
                            list.Add(item);
                        }
                    }
                }
            }

            // [ML] - return default list as its always nice to avoid null reference exceptions on lists =)

            return list;
        }

    }
}