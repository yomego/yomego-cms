﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Yomego.CMS.Umbraco.ModelBuilder.Extensions;

namespace Yomego.CMS.Umbraco.ModelBuilder.ComponentModel.TypeConverters
{
    public class BaseTypeConverter : TypeConverter
    {
        public PublishedContentContext GetContext(ITypeDescriptorContext context)
        {
            var item = context as PublishedContentContext;

            if (item == null)
            {
                throw new Exception("TypeDescriptorContext is not of type required by BaseTypeConverter");
            }

            return item;
        }

        public IList GetListFromPublishContext(PublishedContentContext context)
        {
            var type = typeof(PublishedContentModel);

            if (context.OutputType != null)
            {
                type = context.OutputType;
            }

            var constructedListType = typeof(List<>).MakeGenericType(type);
            var list = (IList)Activator.CreateInstance(constructedListType);

            return list;
        }

        public IList GetListFromOriginalContextType(ITypeDescriptorContext context)
        {
            var publishedContext = GetContext(context);

            return GetListFromPublishContext(publishedContext);
        }

        public PublishedContentModel GetFromPublishContent(IPublishedContent content)
        {
            if (content != null)
            {
                var type = ConverterHelper.FirstFromBaseType<PublishedContentModel>(content.DocumentTypeAlias);

                if (type != null)
                {
                    return content.As(type);
                }
            }

            return null;
        }

        public PublishedContentModel GetFromId(int id)
        {
            var umbracoHelper = ConverterHelper.UmbracoHelper;

            return GetFromPublishContent(umbracoHelper.TypedContent(id));
        }

        public PublishedContentModel GetFromId(string id)
        {
            int idAsInt;

            return int.TryParse(id, out idAsInt) ? GetFromId(idAsInt) : null;
        }
    }
}
