﻿namespace Yomego.CMS.Umbraco.Services.Search.Enums
{
    public enum SearchOrder
    {
        Unknown = 0,
        RankDesc = 1,
        DateCreated = 2,
        PublishDate = 3,
        ManualSort = 4
    }
}
