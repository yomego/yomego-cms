﻿namespace Yomego.CMS.Umbraco.Services.Search.Enums
{
    public enum OperatorEnum
    {
        OR,
        AND,
        NOT
    }
}
