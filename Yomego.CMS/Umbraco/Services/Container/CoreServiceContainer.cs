﻿using Yomego.CMS.Umbraco.Services.Content;
using Yomego.CMS.Umbraco.Services.DataTypes;

namespace Yomego.CMS.Umbraco.Services.Container
{
    public class CoreServiceContainer : Context.Container
    {
        public ContentService Content { get { return Get<ContentService>(); } }

        public DataTypeService DataType { get { return Get<DataTypeService>(); } }
    }
}
