﻿namespace jumps.umbraco.usync.Interfaces
{
    public interface IuSyncService
    {
        void Sync();

        void Save();
    }
}
