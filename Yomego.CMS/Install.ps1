﻿# Install.ps1
param($installPath, $toolsPath, $package, $project)

$projectPath = Split-Path -Parent $project.FullName

# ********************************* Amend ExamineIndex.config

$xmlEi = New-Object xml

# find its path on the file system
$localPathEi = Join-Path $projectPath "config\ExamineIndex.config"

# load Web.config as XML
$xmlEi.Load($localPathEi)

# select the node
$indexSets = $xmlEi.SelectSingleNode("ExamineLuceneIndexSets")

$indexSet = $xmlEi.CreateElement("IndexSet")
$indexSet.SetAttribute("SetName", "WebsiteIndexSet")
$indexSet.SetAttribute("IndexPath", "~/App_Data/TEMP/ExamineIndexes/WebsiteIndex/")

$indexAttribute = $xmlEi.CreateElement("IndexAttributeFields")

$attributeId = $xmlEi.CreateElement("add")
$attributeId.SetAttribute("Name", "id")
$attributeNodeName = $xmlEi.CreateElement("add")
$attributeNodeName.SetAttribute("Name", "nodeName")
$attributeNodeTypeAlias = $xmlEi.CreateElement("add")
$attributeNodeTypeAlias.SetAttribute("Name", "nodeTypeAlias")
$attributeSystemSortOrder = $xmlEi.CreateElement("add")
$attributeSystemSortOrder.SetAttribute("Name", "SystemSortOrder")
$attributeSystemSortOrder.SetAttribute("EnableSorting", "true")
$attributeSystemPublishDate = $xmlEi.CreateElement("add")
$attributeSystemPublishDate.SetAttribute("Name", "SystemPublishDate")
$attributeSystemPublishDate.SetAttribute("EnableSorting", "true")
$attributeContentDatePublished = $xmlEi.CreateElement("add")
$attributeContentDatePublished.SetAttribute("Name", "ContentDatePublished")
$attributeContentDatePublished.SetAttribute("EnableSorting", "true")

$indexAttribute.AppendChild($attributeId)
$indexAttribute.AppendChild($attributeNodeName)
$indexAttribute.AppendChild($attributeNodeTypeAlias)
$indexAttribute.AppendChild($attributeSystemSortOrder)
$indexAttribute.AppendChild($attributeSystemPublishDate)
$indexAttribute.AppendChild($attributeContentDatePublished)

$indexSet.AppendChild($indexAttribute)
$indexSets.AppendChild($indexSet)

# save the Web.config file
$xmlEi.Save($localPathEi)

# ********************************* Amend ExamineSettings.config

$xmlEs = New-Object xml

# find its path on the file system
$localPathEs  = Join-Path $projectPath "config\ExamineSettings.config"

# load Web.config as XML
$xmlEs.Load($localPathEs)

# select the node

$indexProviders = $xmlEs.SelectSingleNode("Examine/ExamineIndexProviders/providers")

$websiteIndexer = $xmlEs.CreateElement("add")
$websiteIndexer.SetAttribute("name", "WebsiteIndexer")
$websiteIndexer.SetAttribute("type", "UmbracoExamine.UmbracoContentIndexer, UmbracoExamine")
$websiteIndexer.SetAttribute("supportUnpublished", "false")
$websiteIndexer.SetAttribute("supportProtected", "true")
$websiteIndexer.SetAttribute("interval", "10")
$websiteIndexer.SetAttribute("analyzer", "Lucene.Net.Analysis.Standard.StandardAnalyzer, Lucene.Net")

$indexProviders.AppendChild($websiteIndexer)

$searchProviders = $xmlEs.SelectSingleNode("Examine/ExamineSearchProviders/providers")

$websiteSearcher = $xmlEs.CreateElement("add")
$websiteSearcher.SetAttribute("name", "WebsiteSearcher")
$websiteSearcher.SetAttribute("type", "UmbracoExamine.UmbracoExamineSearcher, UmbracoExamine")
$websiteSearcher.SetAttribute("analyzer", "Lucene.Net.Analysis.WhitespaceAnalyzer, Lucene.Net")
$websiteSearcher.SetAttribute("enableLeadingWildcards", "true")

$searchProviders.AppendChild($websiteSearcher)


# save the Web.config file
$xmlEs.Save($localPathEs)
