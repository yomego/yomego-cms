﻿using System;
using Yomego.CMS.Context;
using Yomego.CMS.Mvc.Settings;
using Yomego.CMS.Umbraco.Services.Container;

namespace Yomego.CMS
{
    public class App : CoreApp<ServiceContainer>
    {
        private readonly Lazy<WebConfig> _webConfig = new Lazy<WebConfig>();

        public WebConfig WebConfig
        {
            get { return _webConfig.Value; }
        }
    }
}
