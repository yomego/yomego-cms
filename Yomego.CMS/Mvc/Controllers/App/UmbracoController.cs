﻿using System;

namespace Yomego.CMS.Mvc.Controllers.App
{
    public class UmbracoController<TApp> : BaseController where TApp : class
    {
        private Lazy<TApp> LazyApp = new Lazy<TApp>();

        protected TApp App
        {
            get
            {
                return LazyApp.Value;
            }
        }
    }
}
