﻿namespace Yomego.CMS.Mvc.Model.Media
{
    public class ImageCrops
    {
        public Focalpoint focalPoint { get; set; }

        public string src { get; set; }
    }
}